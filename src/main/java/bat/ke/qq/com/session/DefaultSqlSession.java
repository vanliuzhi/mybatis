package bat.ke.qq.com.session;
/*
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　┻　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　永无BUG 　┣┓
 * 　　　　┃　　如来保佑　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┗┻┛　┗┻┛
 * 源码学院-只为培养BAT程序员而生
 * bat.ke.qq.com
 * Monkey老师
 */

import bat.ke.qq.com.binding.MapperMethod;
import bat.ke.qq.com.binding.MapperProxy;
import bat.ke.qq.com.executor.Executor;

import java.lang.reflect.Proxy;

public class DefaultSqlSession implements SqlSession {
  private Configuration configuration;
  private Executor executor;

  public Configuration getConfiguration() {
    return configuration;
  }

  public DefaultSqlSession(Configuration configuration, Executor executor) {
    this.configuration = configuration;
    this.executor = executor;
  }

  public <T> T getMapper(Class<T> type) {
    return (T) Proxy.newProxyInstance(type.getClassLoader(), new Class[]{type},new MapperProxy<>(this,type));
  }
  @Override
  public <T> T selectOne(MapperMethod mapperMethod, Object statement) throws Exception {
    System.out.println();
    return  executor.query(mapperMethod,statement);
  }
}
