package bat.ke.qq.com.executor;/*
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　┻　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　永无BUG 　┣┓
 * 　　　　┃　　如来保佑　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┗┻┛　┗┻┛
 * 源码学院-只为培养BAT程序员而生
 * bat.ke.qq.com
 * Monkey老师
 */


import bat.ke.qq.com.binding.MapperMethod;

public interface Executor {

  <T> T query(MapperMethod method, Object parameter) throws Exception;
}
